import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createRecord, editRecord} from '../../store/actions/index'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import PropTypes from 'prop-types';
import moment from 'moment';


class RecordsNew extends Component {

	componentWillMount() {
		const {record} = this.props;
		if (record) {
			this.props.initialize({...record});
		}
	}

	renderSimpleField(field) {
		const {meta: {touched, error}} = field;
		const className = `form-control ${touched && error ? 'is-invalid' : ''}`;

		return (
			<div className={'form-group'}>
				<label>{field.label}</label>
				<input
					className={className}
					type={field.type}
					{...field.input}
				/>
				<div className="invalid-feedback">
					{touched ? error : ''}
				</div>
			</div>
		)
	}

	renderDatePicker = ({input, label, placeholder, defaultValue, meta}) => (
		<div>
			<label>{label}</label>
			<DatePicker {...input} 	dateFormat={'MM-DD-YYYY'}
						className={`form-group form-control ${meta.touched && meta.error ? 'is-invalid' : ''}`}
						selected={input.value ? moment(input.value, 'MM-DD-YYYY') : null}/>
			<div className="invalid-feedback">
				{meta.touched ? meta.error : ''}
			</div>
		</div>
	);

	onSubmit(values) {
		const {record} = this.props;
		if (record) {
			const {id, date} = record;
			this.props.editRecord(id, date, values, () => {
				this.props.history.push('/');
			});
		} else {
			this.props.createRecord(values, () => {
				this.props.history.push('/');
			});
		}
	}

	render() {
		const {handleSubmit} = this.props;

		return (
			<div className="container pt-3">
				<form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
					<Field
						label="First Name"
						name="firstName"
						component={this.renderSimpleField}
					/>
					<Field
						label="Last Name"
						name="lastName"
						component={this.renderSimpleField}
					/>
					<Field
						label="Points"
						name="points"
						component={this.renderSimpleField}
						type="number"
					/>
					<Field
						label="Date"
						id="Date"
						name="date"
						format={(value, name) => value || null}
						component={this.renderDatePicker}
					/>
					<button type="submit" className="btn btn-primary">Submit</button>
					<Link to="/" className="btn btn-danger">Cancel</Link>
				</form>
			</div>
		)
	}
}

function validate(values) {
	const errors = {};

	if (!values.firstName) {
		errors.firstName = "Enter your first name"
	}
	if (!values.lastName) {
		errors.lastName = "Enter last name!"
	}
	if (!values.points) {
		errors.points = "Enter some points value please";
	} else if (isNaN(values.points)) {
		errors.points = "Only number here";
	}
	if (!values.date) {
		errors.date = "Enter a date"
	}

	return errors;
}

function mapStateToProps({lead}, ownProps) {
	const date = ownProps.match.params.date;
	const id = ownProps.match.params.id;
	return {record: date ? {...lead[date][id], ...{id}, ...{date}} : undefined}
}

RecordsNew.propTypes = {
	record: PropTypes.object,
};


export default reduxForm({validate, form: 'PostsNewForm'})(connect(mapStateToProps, {
	createRecord,
	editRecord
})(RecordsNew));


