import React, {Component} from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {createRecord, deleteRecord} from '../../store/actions/index'
import moment from 'moment'
import RecordIndex from './RecordIndex'

class RecordsIndexContainer extends Component {
	constructor() {
		super();
		this.state = {
			date: moment().format('MM-DD-YYYY')
		};
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(date) {
		this.setState({
			date: date.format('MM-DD-YYYY')
		});
	}

	render() {
		const props = {
			date: this.state.date,
			lead: this.props.lead,
			handleDelete: this.props.deleteRecord,
			handleChange: this.handleChange
		};

		return (
			<RecordIndex {...props}/>
		)
	}
}

function mapStateToProps(state, {lead}) {
	return {lead}
}

RecordsIndexContainer.propTypes = {
	lead: PropTypes.object.isRequired,
};


export default connect(mapStateToProps, {createRecord, deleteRecord})(RecordsIndexContainer)