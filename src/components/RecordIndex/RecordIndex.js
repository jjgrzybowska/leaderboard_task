import React from 'react'
import {Link} from 'react-router-dom';
import _ from 'lodash';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import moment from 'moment'

const RecordsIndex = ({date, lead, handleDelete, handleChange}) => {

	const renderPosts = () => {
		if (lead[date]) {
			return _.sortBy(Object.values(lead[date]), 'points').reverse().map((r) => {
				return (
					<li className="list-group-item d-flex flex-row justify-content-between" key={r.id}>

						<div className="d-flex flex-row">
							<div className="mr-2">
								<span className="badge badge-primary">{r.points}</span>
							</div>

							<div className="d-flex flex-row">
								<div className="mr-2">{r.firstName}</div>
								<div className="mr-2">{r.lastName}</div>
							</div>
						</div>

						<div className={"btn-group btn-group-sm"} role="group">
							<button className={"btn btn-primary btn-sm"} >
								<Link to={`${date}/${r.id}`} style={{textDecoration:'none', color: 'white'}}>
									EDIT
								</Link>
							</button>
							<button className={"btn btn-danger btn-sm"} onClick={() => {
								handleDelete(r.id,date)
							}}>DELETE
							</button>
						</div>
					</li>
				)
			})
		}
	};

	return (
		<div>
			<div className={"d-flex flex-row justify-content-between align-items-center mr-4 ml-4"}>
				<div>SCORES:</div>
				<DatePicker
					selected={moment(date,'MM-DD-YYYY')}
					onChange={handleChange}
					className="form-control"
					includeDates={Object.keys(lead || []).map((el) => {
						return moment(el,'MM-DD-YYYY')
					})}
				/>
				<Link className={"btn btn-primary"} to="/new">
					+ ADD NEW
				</Link>
			</div>
			<ul className="list-group">
				{renderPosts()}
			</ul>
		</div>
	)
};


RecordsIndex.propTypes = {
	date: PropTypes.string.isRequired,
	lead: PropTypes.object.isRequired,
	handleDelete: PropTypes.func.isRequired,
	handleChange: PropTypes.func.isRequired,
};

export default RecordsIndex;