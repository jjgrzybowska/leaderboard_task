import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import {loadState, saveState} from './localStorage';
import reducers from './store/reducers';
import RecordsNew from './components/RecordNew/RecordsNew';
import throttle from 'lodash/throttle';
import RecordIndexContainer from "./components/RecordIndex/RecordIndexContainer";

const persistedState = loadState();
const store = createStore(reducers, persistedState);

store.subscribe(throttle(()=>{
	saveState({lead: store.getState().lead})
}, 1000));


ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<div>
				<header>
					<div className="navbar navbar-dark bg-dark box-shadow">
						<h3 className="text-light bg-dark">Leaderboard</h3>
					</div>
				</header>
				<Switch>
					<Route path="/new" component={RecordsNew}/>
					<Route path="/:date/:id" component={RecordsNew}/>
					<Route path="/" component={RecordIndexContainer}/>
				</Switch>
			</div>
		</BrowserRouter>
	</Provider>
	, document.querySelector('#root'));