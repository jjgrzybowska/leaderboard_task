import {v4} from 'node-uuid';

export const CREATE_RECORD = 'create_record';
export const EDIT_RECORD = 'edit_record';
export const DELETE_RECORD = 'delete_record';



export function createRecord(values, callback) {
	callback();
	return {
		type: CREATE_RECORD,
		values,
		id: v4()
	}
}

export function deleteRecord(id, date) {
	return {
		type: DELETE_RECORD,
		id,
		date
	}
}

export function editRecord(id, date,values ,callback) {
	callback();
	return {
		type: EDIT_RECORD,
		id,
		date,
		values
	}
}

