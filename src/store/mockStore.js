import faker from 'faker'
import moment from 'moment'

const fakeRecord = () => {
	const number = faker.random.number({min: 5, max: 15});
	const mnth = moment().startOf('month').format('MM-DD-YYYY');
	const tmr = moment().add(+1, 'day').format('MM-DD-YYYY');

	let date = moment(faker.date.between(mnth, tmr)).format('MM-DD-YYYY');
	return {
		[date]: createFakeCollection(number, () => {
			return fakeUser(date)
		})
	};
};

const fakeUser = (date) => {

	let id = faker.random.uuid();
	return {
		[id]: {
			firstName: faker.name.firstName(),
			lastName: faker.name.lastName(),
			points: faker.random.number({min: 0, max: 1000}),
			id
		}
	};

};

const createFakeCollection = (number, callback) => {
	let store = {};
	for (let i = 0; i < number; i++) {
		store = {...store, ...callback()}
	}
	return store;
};

const createFakeLead = () => {
	return createFakeCollection(40, fakeRecord)
};

export default createFakeLead;