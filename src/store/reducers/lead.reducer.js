import {CREATE_RECORD, DELETE_RECORD, EDIT_RECORD} from '../actions/index'
import omit from 'lodash/omit';
import createFakeLead from '../mockStore';

const initialState = createFakeLead();

export default function (state = initialState, action) {
	const {id, date, values} = action;
	const formDate = values ? values.date : "";

	switch (action.type) {
		case CREATE_RECORD:
			const init = (state[formDate]) ? {} : Object.assign(state, {[formDate]: {}});
			return {...init,
				 ...state,
				[formDate]: {...state[formDate], [id]: {...state[formDate][id], ...{...values, id}}}
			};
		case DELETE_RECORD:
			let deletedId = omit(state[date], id);

			if (Object.keys(state[date]).length === 1) {
				return {...omit(state, date)}
			}

			return {...state, [date]: deletedId};
		case EDIT_RECORD:
			deletedId = {};
			if (date !== formDate) {
				deletedId = omit(state[date], id);
				const init = (state[formDate]) ? {} : Object.assign(state, {[formDate]: {}});
				if (Object.keys(deletedId).length === 0) {
					return {
						...init, ...omit(state, date),
						[formDate]: {...state[formDate], [id]: {...state[formDate][id], ...values}}
					}
				}
			}
			return {
				...state,
				[date]: deletedId,
				[formDate]: {...state[formDate], [id]: {...state[formDate][id], ...values}}
			};

		default:
			return state;
	}
}

