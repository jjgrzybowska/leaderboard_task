import {combineReducers} from 'redux';
import PostsReducer from './lead.reducer'
import {reducer as formReducer} from 'redux-form'

const rootReducer = combineReducers({
	lead: PostsReducer,
	form: formReducer
});

export default rootReducer;
